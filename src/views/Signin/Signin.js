import React, { Component } from 'react';
import { Redirect } from "react-router-dom";
import { connect } from 'react-redux';
import { ActSignin } from '../../service/Signin/actions';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Container from '@material-ui/core/Container';
import Cookies from 'universal-cookie';
const cookies = new Cookies();

class Signin extends Component {
 constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      redirectToReferrer: false,
      message: null
    }
  }
  
  handleFieldChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  hendleClick = (event) => {
    event.preventDefault();
    
    if (this.state.email !== "" || this.state.password !== "") {
      this.props.ActSignin(this.state.email, this.state.password, (data, error) => {
        if (data) {
          
          cookies.set('email', data.email, { path: '/' });
          cookies.set('Fname', data.TFname, { path: '/' });
          cookies.set('Lname', data.TLname, { path: '/' });
          cookies.set('id', data.TID, { path: '/' });
          this.setState({ redirectToReferrer: true });
        
          
        }
        else {
          this.setState({ message: (<a>Login failed. your email or password is wrong</a>) });
        }
      });
    } else {
      this.setState({ message: (<a>Login failed. Please enter your email or password</a>) })
    }
  } 

  render() {
    const { redirectToReferrer } = this.state;
    const root = {
      flexGrow: 1
    }
    const title = {
      flexGrow: 1
    }
    const paper = {
      marginTop: '80 px',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center'
    }
    const avatar = {
      margin: '80 px',
      backgroundColor: 'primary',
    }
    const form = {
      width: '100%', // Fix IE 11 issue.
      marginTop: '80 px',
    }
    const submit = {
      margin: '80 px',
    }

    // console.log(this.state.redirectToReferrer);
    if (redirectToReferrer) return <Redirect to="/user" />;

    return (
      <div style={root}>

        <AppBar position="static" style={{ backgroundColor: '#FFA602',}}>
          <Toolbar>

            <Typography variant="h6" style={title} >
              KMITL
          </Typography>

          </Toolbar>
        </AppBar>
        <Container component="main" maxWidth="xs">
          <CssBaseline />
          <br />
          <br />
          <div style={paper} >
            <Avatar  >
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5" >
              Sign in
            </Typography>
            <form noValidate>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                type="text"
                autoComplete="email"
                autoFocus
                value={this.state.email} onChange={this.handleFieldChange}
              />
              <TextField
              id="standard-password-input"
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                name='password' value={this.state.password} onChange={this.handleFieldChange}
              />

              <p style={{ color: "red" }}>{this.state.message}</p>
              <Button
                type="submit"
                fullWidth
                variant="contained" df
                color="primary"
                style={{ backgroundColor: '#FFA602',}}
                onClick={this.hendleClick}
              >
                Sign In
              </Button>
              <br />
              <br />
              {/* <Grid container>
                <Grid item xs>
                  <Link href="#" variant="body2">
                    Forgot password?
              </Link>
                </Grid>
                <Grid item>
                  <Link href="#" variant="body2">
                    {"Don't have an account? Sign Up"}
                  </Link>
                </Grid>
              </Grid> */}
            </form>
          </div>
          <Box mt={8}>

          </Box>
        </Container>


      </div>
    );
  }
}

export default connect(null, { ActSignin })(Signin);