import React, { Component } from 'react';
import GridItem from "../../components/Grid/GridItem.js";
import GridContainer from "../../components/Grid/GridContainer.js";
import Card from "../../components/Card/Card.js";
import CardBody from "../../components/Card/CardBody.js";
import Grid from '@material-ui/core/Grid';
import {
  grayColor,
  defaultFont
} from "../../assets/jss/material-dashboard-react.js";
import FeaturedPost from './FeaturedPost';
import DeleteIcon from '@material-ui/icons/Delete';
import { render } from "@testing-library/react";
import { ActShow } from '../../service/Show/action';
import { ActDelete } from '../../service/Delete/action';
import { connect } from 'react-redux';
import Cookies from 'universal-cookie';
const cookies = new Cookies();
class MyBooking extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tid:  cookies.get('id'),
      booking : [],
      bookingId : null

    }
  }
  componentDidMount() {
    this.props.ActShow(this.state.tid, (data, error) => {
      if (data) {
        
        this.setState({
          booking : data,

        });
      }
      else {
        this.setState({ message: (<a>Login failed. your email or password is wrong</a>) });
      }
    });
    
  }
  delete = async (booking) => {
    await this.setState({bookingId : booking})
    this.props.ActDelete(this.state.bookingId,(data, error) => {
      if (data) {window.location.reload();
      }
    
    });

  }
  render() {
    return (
      <div>
        <GridContainer>
          <GridItem xs={12} sm={12} md={12}>
            

              <CardBody>
                <div>
                  <Grid container spacing={2}>
                     {this.state.booking.map(booking => (
                      <FeaturedPost key={booking.BookingId} booking={booking} handleClick={this.delete}/>
                    ))} 
                  </Grid>
                </div>


              </CardBody>
            
          </GridItem>

        </GridContainer>
      </div>
    );
  }
}
export default connect(null, { ActShow , ActDelete})(MyBooking);
