import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Hidden from '@material-ui/core/Hidden';
import Button1 from "../../components/CustomButtons/Button.js";
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';


const useStyles = makeStyles({
  card: {
    display: 'flex',
  },
  cardDetails: {
    flex: 1,
  },
  cardMedia: {
    width: 160,
  },
  justifyt : {
    justifyContent: "center",
    display: "flex",
    alignItems: "center",
},
  colors :{ color :"#f44336"

}
});

export default function FeaturedPost(props) {
  const classes = useStyles();
  const { booking } = props;
   const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Grid item xs={12} sm={12} md={4}>
     
        <Card className={classes.card}>
        {/* <Hidden xsDown>
            <CardMedia className={classes.cardMedia} image={post.image} title={post.imageTitle} />
          </Hidden> */}
          <div className={classes.cardDetails}>
            <CardContent>
              <Typography component="h2" variant="h5">
                เลขที่การจอง {booking.BookingId}
              </Typography>
              <Typography component="h3" variant="h6">
                อาคาร {booking.Build}
              </Typography>
              <Typography component="h3" variant="h6">
                ห้อง {booking.Roomname}
              </Typography>
              <Typography variant="subtitle1" paragraph>
                วันที่ {booking.Dates}
              </Typography>
              <Typography variant="subtitle1" paragraph>
                เวลา {booking.TimeStart} - {booking.TimeEnd}
              </Typography>
              <Typography variant="subtitle1" paragraph>
                วิชา {booking.Subjects}
              </Typography>
              

              <Button1
              color="danger" 
              className={classes.justifyt}
            
              onClick={handleClickOpen}>
                Delete</Button1>
                <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Delete this booking"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
          Are you sure you want to Delete This Booking
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button variant="outlined" onClick={handleClose} color="primary">
            Cancle
          </Button>
          
          <Button variant="outlined" onClick={()=> props.handleClick(props.booking.BookingId)} className={classes.colors} autoFocus>
            Confirm
          </Button>
        </DialogActions>
      </Dialog>

 
            </CardContent>
          </div>
         
        </Card>
     
    </Grid>
  );
}

FeaturedPost.propTypes = {
  booking: PropTypes.object,
};