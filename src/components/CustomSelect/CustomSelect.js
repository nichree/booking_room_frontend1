import React from 'react';
import classNames from "classnames";
import PropTypes from "prop-types";
// @material-ui/core components
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
// core components
import styles from "../../assets/jss/material-dashboard-react/components/customInputStyle.js";
const useStyles = makeStyles(styles);
export default function CustomInput(props) {
    const classes = useStyles();
    const {
      formControlProps,
      labelText,
      id,
      labelProps,
      inputProps,
      MenuProps
    } = props;
    
    const [age, setAge] = React.useState('');
    const [open, setOpen] = React.useState(false);
  
    const handleChange = event => {
      setAge(event.target.value);
    };
  
    const handleClose = () => {
      setOpen(false);
    };
  
    const handleOpen = () => {
      setOpen(true);
    };
    return (
        <FormControl 
        {...formControlProps}
        className={formControlProps.className + " " + classes.formControl}
        >
        {labelText !== undefined ?(
        <InputLabel className={classes.labelRoot + labelClasses}
        htmlFor={id}
          {...labelProps}
          > {labelText}</InputLabel>) : null}
          
        <Select
           classes={{
            root: marginTop,
            disabled: classes.disabled,
            underline: underlineClasses
          }}
          id={id}
          
        
          open={open}
          onClose={handleClose}
          onOpen={handleOpen}
          value={age}
          onChange={handleChange}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value={10}>Ten</MenuItem>
          <MenuItem value={20}>Twenty</MenuItem>
          <MenuItem value={30}>Thirty</MenuItem>
        </Select>
      </FormControl>
    );
    
}
CustomInput.propTypes = {
    labelText: PropTypes.node,
    labelProps: PropTypes.object,
    id: PropTypes.string,
    formControlProps: PropTypes.object,
    
};