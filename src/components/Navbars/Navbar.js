import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Hidden from "@material-ui/core/Hidden";
// @material-ui/icons
import Menu from "@material-ui/icons/Menu";
// core components
import AdminNavbarLinks from "./AdminNavbarLinks.js";
import RTLNavbarLinks from "./RTLNavbarLinks.js";
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import styles from "../../assets/jss/material-dashboard-react/components/headerStyle.js";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Redirect } from "react-router-dom";
import Cookies from 'universal-cookie';
const cookies = new Cookies();

const useStyles = makeStyles(styles);

export default function Header(props) {
  const classes = useStyles();
  const colortext = {
    color: "#FFFFFF"
  }
  function makeBrand() {
    var name;
    props.routes.map(prop => {
      if (window.location.href.indexOf(prop.layout + prop.path) !== -1) {
        name = props.rtlActive ? prop.rtlName : prop.name;
      }
      return null;
    });
    return name;
  }
  const [open, setOpen] = React.useState(false);
  
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const handleClick = () => {
  
    cookies.remove('email', { path: '/' })
    cookies.remove('Fname' ,{ path: '/' });
    cookies.remove('Lname' ,{ path: '/' });
    cookies.remove('id' ,{ path: '/' });
   
    
  };
  const { color } = props;
  const appBarClasses = classNames({
    [" " + classes[color]]: color
  });
  
  return (
    <AppBar className={classes.appBar + appBarClasses} >
      <Toolbar className={classes.container}>
        <div className={classes.flex}>
          {/* Here we create navbar brand, based on route name */}
          <Typography style={colortext} >
            {makeBrand()}
          </Typography>

        </div>
        <div>
          <Button style={{
            justifyContent: "flex-end",
            display: "flex",
            alignItems: "flex-end",
            color: '#FFFFFF',
            border: '1px solid #FFFFFF',
            backgroundColor: '#FFA602'
          }}
            onClick={handleClickOpen} >
            &nbsp;Log Out&nbsp;
          </Button>
          <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">{"Delete this booking"}</DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                Are you sure you want to Log out
          </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button variant="outlined" onClick={handleClose} color="primary">
                Cancle
          </Button>

              <Button variant="outlined" onClick={handleClick} style={{
                color: '#f44336',
                border: '1px solid #f44336',
                backgroundColor: '#FFFFFF'
              }} autoFocus
              href = '/' >
                Confirm
          </Button>
            </DialogActions>
          </Dialog>
        </div>
        <Hidden smDown implementation="css">
          {props.rtlActive ? <RTLNavbarLinks /> : <AdminNavbarLinks />}
        </Hidden>
        <Hidden mdUp implementation="css">
          <IconButton
            color="orange"
            aria-label="open drawer"
            onClick={props.handleDrawerToggle}
          >

            <Menu />
          </IconButton>
        </Hidden>
      </Toolbar>
    </AppBar>
  );
}

Header.propTypes = {
  color: PropTypes.oneOf(["primary", "info", "success", "warning", "danger"]),
  rtlActive: PropTypes.bool,
  handleDrawerToggle: PropTypes.func,
  routes: PropTypes.arrayOf(PropTypes.object)
};
