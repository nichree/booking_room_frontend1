import {DELETE} from '..//actions/type';
export const UserReducer = (state = [], action) => {
    switch(action.type){
        case DELETE:
            return action.payload;
        default:
            return state;
    }
}