import axios from 'axios';
import { useCallback } from 'react';
import {BOOKING} from './type';

export const ActBooking = (build, timestart,room,timeend,id,dates,subject, callback = null) => {
    return dispatch =>{
       
        console.log(build, timestart,room,timeend,id,dates,subject)
        const API_PATH = 'http://203.150.243.108:8086/Booking';
        axios(API_PATH, {
            method: 'PUT',
            headers: {
                'content-type': 'application/json'
            },
            data:{
                Build : build,
                Subjects : subject,
                TID : id,
                TimeStart :timestart,
                TimeEnd : timeend,
                Dates  : dates,
                RoomID : room,
                }
        })
    .then(reponse => {
        if(reponse.data){
            dispatch({type:BOOKING, payload: reponse.data});
        }
        if(callback != null){
            callback(reponse.data,null);
        }
    })
        .catch(error => {
            console.log(error);
            callback(null,error);
    
        })
    }
}