import axios from 'axios';
import { useCallback } from 'react';
import {ROOM} from './type'; 

export const ActRoom = (build, callback = null) => {
    return dispatch =>{
 
        const API_PATH = 'http://203.150.243.108:8086/SelectRoom';
        axios(API_PATH, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            data:{
                Build : build,
               
            }
        })
    
    .then(reponse => {
        if(reponse.data){
            dispatch({type:ROOM, payload: reponse.data});
           
        }
        if(callback != null){
            callback(reponse.data,null);
        }
    })
        .catch(error => {
            console.log(error);
            callback(null,error);
    
        })
    }
}