import {ROOM} from '../actions/type';
export const UserReducer = (state = [], action) => {
    switch(action.type){
        case ROOM:
            return action.payload;
        default:
            return state;
    }
}