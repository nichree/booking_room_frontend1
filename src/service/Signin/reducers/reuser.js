import {SIGNIN} from '..//actions/type';
export const UserReducer = (state = [], action) => {
    switch(action.type){
        case SIGNIN:
            return action.payload;
        default:
            return state;
    }
}