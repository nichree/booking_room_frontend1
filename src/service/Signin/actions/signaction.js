import axios from 'axios';
import { useCallback } from 'react';
import {SIGNIN} from './type';

export const ActSignin = (email, password, callback = null) => {
    return dispatch =>{
        // var boyFormData = new FormData();
        // // boyFormData.set('username', username);
        // // boyFormData.set('password', password);

        // http://161.246.58.241:8000/clinic-api-php/RestController.php?view=selectUserByUsernamePassword
        
        const API_PATH = 'http://203.150.243.108:8086/Signin';
        axios(API_PATH, {
            method: 'POST',
            headers: {
                'content-type': 'application/json'
            },
            data:{
                email: email,
                password: password}
        })
    .then(reponse => {
        if(reponse.data){
            dispatch({type:SIGNIN, payload: reponse.data});
        }
        if(callback != null){
            callback(reponse.data,null);
        }
    })
        .catch(error => {
            console.log(error);
            callback(null,error);
    
        })
    }
}