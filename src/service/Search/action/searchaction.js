import axios from 'axios';
import { useCallback } from 'react';
import {SEARCH} from './type'; 

export const ActSearch = (build,status,timestr,timeend,cap,date, callback = null) => {
    return dispatch =>{
        
        const API_PATH = 'http://203.150.243.108:8086/Search';
        axios(API_PATH, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            data:{
                Build: build,
                TimeStart : timestr,
                TimeEnd : timeend,
                Dates : date,
                Capacity : cap,
                Status : status,
    
            }
            
        })
    
    .then(reponse => {
        if(reponse.data){
            dispatch({type:SEARCH, payload: reponse.data});
           
        }
        if(callback != null){
            callback(reponse.data,null);
        }
    })
        .catch(error => {
            console.log(error);
            callback(null,error);
    
        })
    }
}