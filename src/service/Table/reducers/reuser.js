import {TABLE} from '../actions/type';
export const UserReducer = (state = [], action) => {
    switch(action.type){
        case TABLE:
            return action.payload;
        default:
            return state;
    }
}