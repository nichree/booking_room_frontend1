import axios from 'axios';
import { useCallback } from 'react';
import {TABLE} from './type'; 

export const ActTable = (build,room, callback = null) => {
    return dispatch =>{
       
        const API_PATH = 'http://203.150.243.108:8086/SelectTable';
        axios(API_PATH, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            data:{
                Build : build,
                Roomname : room
            }
        })
    
    .then(reponse => {
        if(reponse.data){
            dispatch({type:TABLE, payload: reponse.data});
           
        }
        if(callback != null){
            callback(reponse.data,null);
        }
    })
        .catch(error => {
            console.log(error);
            callback(null,error);
    
        })
    }
}