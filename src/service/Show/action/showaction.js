import axios from 'axios';
import { useCallback } from 'react';
import {SHOW} from './type'; 

export const ActShow = (tid, callback = null) => {
    return dispatch =>{
       
        const API_PATH = 'http://203.150.243.108:8086/SelectBooking';
        axios(API_PATH, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            data:{
                TID: tid,
            }
        })
    
    .then(reponse => {
        if(reponse.data){
            dispatch({type:SHOW, payload: reponse.data});
           
        }
        if(callback != null){
            callback(reponse.data,null);
        }
    })
        .catch(error => {
            console.log(error);
            callback(null,error);
    
        })
    }
}