import {SHOW} from '..//actions/type';
export const UserReducer = (state = [], action) => {
    switch(action.type){
        case SHOW:
            return action.payload;
        default:
            return state;
    }
}