import React, { Component } from 'react';
import { Redirect } from "react-router-dom";
import { connect } from 'react-redux';
import { ActSignin } from '../actions';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Container from '@material-ui/core/Container';
import { red } from '@material-ui/core/colors';

class Signin extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "top_siripat@hotmail.com",
      password: "54788",
      redirectToReferrer: false,
      message: null
    }
  }
  handleFieldChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  hendleClick = (event) => {
    event.preventDefault();
    if (this.state.email !== "" || this.state.password !== "") {
      this.props.ActSignin(this.state.email, this.state.password, (data, error) => {
        if (data) {
          this.setState({ redirectToReferrer: true });
        }
        else {
          this.setState({ message: (<a>Login failed. your email or password is wrong</a>) });
        }
      });
    } else {
      this.setState({ message: (<a>Login failed. Please enter your email or password</a>) })
    }
  }



  render() {
    const { redirectToReferrer } = this.state;
    const root = {
      flexGrow: 1
    }
    const title = {
      flexGrow: 1
    }
    const paper = {
      marginTop: '80 px',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center'
    }
    const avatar = {
      margin: '80 px',
      backgroundColor: 'primary',
    }
    const form = {
      width: '100%', // Fix IE 11 issue.
      marginTop: '80 px',
    }
    const submit = {
      margin: '80 px',
    }

    // console.log(this.state.redirectToReferrer);
    if (redirectToReferrer) return <Redirect to="/routebasic" />;

    return (
      <div style={root}>

        <AppBar position="static">
          <Toolbar>

            <Typography variant="h6" style={title} >
              KMITL
          </Typography>

          </Toolbar>
        </AppBar>
        <Container component="main" maxWidth="xs">
          <CssBaseline />
          <br/>
          <br/>
          <div style={paper} >
            <Avatar  >
              <LockOutlinedIcon  />
            </Avatar>
            <Typography component="h1" variant="h5" >
              Sign in
            </Typography>
            <form noValidate>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                autoFocus
                value={this.state.email} onChange={this.handleFieldChange}
              />
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                type='text' name='password' value={this.state.password} onChange={this.handleFieldChange}
              />
              <FormControlLabel
                control={<Checkbox value="remember" color="primary" />}
                label="Remember me"
              />
              <Button
                type="submit"
                fullWidth
                variant="contained" df
                color="primary"
                onClick={this.hendleClick}
              >
                Sign In
              </Button>
              <Grid container>
                <Grid item xs>
                  <Link href="#" variant="body2">
                    Forgot password?
              </Link>
                </Grid>
                <Grid item>
                  <Link href="#" variant="body2">
                    {"Don't have an account? Sign Up"}
                  </Link>
                </Grid>
              </Grid>
            </form>
          </div>
          <Box mt={8}>

          </Box>
        </Container>


      </div>
    );
  }
}

export default connect(null,{ActSignin})(Signin);