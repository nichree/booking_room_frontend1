import React from 'react';
// import logo from './logo.svg';
import './App.css';
import {
  BrowserRouter,
  Switch,
  Route
} from "react-router-dom";
import Signin from './views/Signin/Signin'
import Main from './Main/Main'
// import Cookies from 'universal-cookie';

class App extends React.Component{
  
  render() {
    // const cookies = new Cookies();
    // cookies.set('myCat', 'Pacman', { path: '/' });
    // console.log(cookies.get('myCat'));
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Signin}></Route>
          <Route path="/user" component={Main}></Route>
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
