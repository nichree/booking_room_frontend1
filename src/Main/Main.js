import React from 'react';
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import "../assets/css/material-dashboard-react.css?v=1.8.0";
import Admin from '../layouts/Admin.js'

class Main extends React.Component {

    render() {
        const hist = createBrowserHistory();
        return (
            <Router history={hist}>
            <Switch>
              <Route path="/user" component={Admin} />
              <Redirect from="/" to="/user/booking" />
            </Switch>
          </Router>
        )
    }
}

export default Main;